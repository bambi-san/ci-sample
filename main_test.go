package main_test

import (
  "github.com/stretchr/testify/assert"
  "testing"
)


func TestAdd(t *testing.T) {
  a := 1
  b := 2

  result := a + b

  assert.Equal(t, 3, result)
}

